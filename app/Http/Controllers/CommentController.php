<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TheLoai;
use App\LoaiTin;
use App\TinTuc;
use App\Comment;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function getComment($id){
        $theloai = TheLoai::all();
        $loaitin = LoaiTin::all();
        $tintuc=TinTuc::find($id);
        $comment = Comment::find($id);

        return view('admin.tintuc.comment',compact('tintuc','theloai','loaitin','comment'));
    }

    public function getXoa($id){
        $comment= Comment::find($id);
        $idTinTuc = $comment->tintuc->id;
        $comment->delete();

        return redirect('admin/comment/danhsach/'.$idTinTuc)->with('thongbao','Success');
    }

    public function postComment(Request $request,$id){
        $idTinTuc = $id;
        $comment = new Comment;
        $tintuc = TinTuc::find($id);
        $comment->idTinTuc = $idTinTuc;
        $comment->idUser = Auth::user()->id;
        $comment->NoiDung = $request->NoiDung;
        $comment->save();
        return redirect()->route('urlTinTuc',[$id,$tintuc->TieuDeKhongDau])->with('thongbao','Success');
        // return redirect("tintuc/$id/".$tintuc->TieuDeKhongDau.".html")->with('thongbao','Success');
    }
}
