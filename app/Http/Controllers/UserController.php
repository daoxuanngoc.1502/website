<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    public function getDanhSach(){
        $user = User::all();
        return view("admin.user.danhsach",compact('user'));
    }

    public function getThem(){
        return view('admin.user.them');
    }

    public function postThem(Request $request){
        $this->validate($request,[
            'name'=>'required|min:3',
            'email'=>'required',
            'password'=>'required|min:3|max:50',
            'passwordAgain'=>'required|same:password'
        ],[
            'name.required'=>'Bạn chưa nhập tên',
            'name.min'=>'Tên người dùng phải có ít nhất 3 ký tự',
            'email.required'=>'Bạn chưa nhập email',
            'password.required'=>'Bạn chưa nhập mật khẩu',
            'password.min'=>'Mật khẩu phải có ít nhất 3 ký tự',
            'password.max'=>'Mật khẩu tối đa 50 ký tự',
            'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
            'passwordAgain.same'=>'Mật khẩu không khớp'
        ]);
        $user = new User;
        $user->name = $request->name;
        if($request->KyDanh == ""){
            $user->KyDanh = "anonymous";
        }else{
            $user->KyDanh = $request->KyDanh;
        }
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->quyen = $request->quyen;
        $user->provider = 'AdminCreate';

        $user->save();
        return redirect('admin/user/them')->with('thongbao','Success!!');
    }


    public function getSua($id){
        $user = User::find($id);
        return view('admin.user.sua',compact('user'));
    }

    public function postSua(Request $request,$id){
        $this->validate($request,[
            'name'=>'required|min:3',
        ],[
            'name.required'=>'Bạn chưa nhập tên',
            'name.min'=>'Tên người dùng phải có ít nhất 3 ký tự',
        ]);
        $user = User::find($id);
        $user->name = $request->name;
        $user->KyDanh = $request->KyDanh;
        $user->quyen = $request->quyen;
            if($request->password != ""){
                $this->validate($request,[
                    'password'=>'required|min:3|max:50',
                    'passwordAgain'=>'required|same:password'
                ],[
                    'password.required'=>'Bạn chưa nhập mật khẩu',
                    'password.min'=>'Mật khẩu phải có ít nhất 3 ký tự',
                    'password.max'=>'Mật khẩu tối đa 50 ký tự',
                    'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
                    'passwordAgain.same'=>'Mật khẩu không khớp'
                ]);
                $user->password = bcrypt($request->password);
            }
        $user->save();
        return redirect('admin/user/them')->with('thongbao','Success!!');
    }

    public function getXoa($id){
        $user = User::find($id);
        $user->delete();

        return redirect('admin/user/danhsach')->with('thongbao','Success!!');
    }

    public function getDangNhapAdmin(){
        return view('admin.login');
    }

    public function postDangNhapAdmin(Request $request){
        $this->validate($request,[
            'password'=>'min:3|max:50',
        ],[
            'password.min'=>'Mật khẩu phải có ít nhất 3 ký tự',
            'password.max'=>'Mật khẩu tối đa 50 ký tự',
        ]);
        $data = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (Auth::attempt($data)) {
            return redirect('admin/theloai/danhsach');
        }else{
            return redirect('admin/dangnhap')->with('thongbao','Sai tài khoản hoặc mật khẩu');
        }

    }

    public function getDangXuatAdmin(){
        Auth::logout();
        return redirect('admin/dangnhap');
    }
}
