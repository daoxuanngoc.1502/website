<?php

namespace App\Http\Controllers;

use App\TinTuc;
use Illuminate\Http\Request;
use App\LoaiTin;
use App\TheLoai;
use App\Comment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class TinTucController extends Controller
{
    public function getDanhSach(){

            $tintuc = TinTuc::orderBy('id','DESC')->get();
            return view('admin.tintuc.danhsach',compact('tintuc'));

    }

    public function getThem(){

            $theloai = TheLoai::all();
            $loaitin = LoaiTin::all();
            return view('admin.tintuc.them',compact('theloai','loaitin'));

    }

    public function postThem(Request $request){
        $this->validate($request,[
            'LoaiTin'=>'required',
            'TieuDe'=>'required|min:3|unique:TinTuc,TieuDe',
            'TomTat'=>'required',
            'NoiDung'=>'required',
        ],[
            'LoaiTin.required'=>'Bạn chưa nhập loại tin',
            'TieuDe.required'=>'Bạn chưa nhập tiêu đề',
            'TieuDe.min'=>'Tên thể loại phải có độ dài từ 3 -> 100 ký tự',
            'TieuDe.unique'=>'Tiêu đề đã tồn tại',
            'TomTat.required'=>'Bạn chưa nhập tóm tắt',
            'NoiDung.required'=>'Bạn chưa nhập tóm tắt',
        ]);

        $tintuc= new TinTuc();
        $tintuc->TieuDe = $request->TieuDe;
        $tintuc->TomTat = $request->TomTat;
        $tintuc->TieuDeKhongDau = changeTitle($request->TieuDe);
        $tintuc->idLoaiTin = $request->LoaiTin;
        $tintuc->NoiDung = $request->NoiDung;
        $tintuc->Status = 0;
        $tintuc->SoLuotXem = 0;
        $tintuc->KyDanh = $request->KyDanh;
        $tintuc->Video = $request->Video;

        if($request->hasFile('Hinh'))
        {
            $file = $request->file('Hinh');
            $name = $file -> getClientOriginalName();
            $Hinh = Str::random(4)."_".$name;
            while(file_exists("upload/tintuc/".$Hinh)){
                $Hinh = Str::random(4)."_".$name;
            }
            $file -> move("upload/tintuc",$Hinh);
            $tintuc->Hinh = $Hinh;
        }else{
            $tintuc->Hinh = "";
        }

        $tintuc->save();

        return redirect('admin/tintuc/them')->with('thongbao','Chờ Duyệt');
    }


    public function getSua($id){
        $theloai = TheLoai::all();
        $loaitin = LoaiTin::all();
        $comment = Comment::all();
        $tintuc=TinTuc::find($id);
        return view('admin.tintuc.sua',compact('tintuc','theloai','loaitin','comment'));
    }

    public function postSua(Request $request,$id){
        $tintuc=TinTuc::find($id);
        $this->validate($request,[
            'LoaiTin'=>'required',
            'TieuDe'=>'required|min:3',
            'TomTat'=>'required',
            'NoiDung'=>'required',
        ],[
            'LoaiTin.required'=>'Bạn chưa nhập loại tin',
            'TieuDe.required'=>'Bạn chưa nhập tiêu đề',
            'TieuDe.min'=>'Tên thể loại phải có độ dài từ 3 -> 100 ký tự',
            'TomTat.required'=>'Bạn chưa nhập tóm tắt',
            'NoiDung.required'=>'Bạn chưa nhập tóm tắt',
        ]);

        $tintuc->TieuDe = $request->TieuDe;
        $tintuc->TomTat = $request->TomTat;
        $tintuc->TieuDeKhongDau = changeTitle($request->TieuDe);
        $tintuc->idLoaiTin = $request->LoaiTin;
        $tintuc->NoiDung = $request->NoiDung;
        $tintuc->Video = $request->Video;

        if($request->hasFile('Hinh'))
        {
            $file = $request->file('Hinh');
            $name = $file -> getClientOriginalName();
            $Hinh = Str::random(4)."_".$name;
            while(file_exists("upload/tintuc/".$Hinh)){
                $Hinh = Str::random(4)."_".$name;
            }
            $file -> move("upload/tintuc/",$Hinh);
            Storage::delete("upload/tintuc/$tintuc->$Hinh");
            // unlink("upload/tintuc/$tintuc->$Hinh");
            $tintuc->Hinh = $Hinh;
        }

        $tintuc->save();
        return redirect('admin/tintuc/sua/'.$id)->with('thongbao','Success');
    }

    public function getXoa($id){
        $tintuc= TinTuc::find($id);
        $tintuc->delete();

        return redirect('admin/tintuc/danhsach')->with('thongbao','Success');
    }

    public function getDuyet($id){
        $tintuc= TinTuc::find($id);
        $tintuc->Status = 1;
        $tintuc->save();

        return redirect('admin/tintuc/danhsach')->with('thongbao','Success');
    }

    public function getHuyDuyet($id){
        $tintuc= TinTuc::find($id);
        $tintuc->Status = 0;
        $tintuc->save();

        return redirect('admin/tintuc/danhsach')->with('thongbao','Success');
    }
}
