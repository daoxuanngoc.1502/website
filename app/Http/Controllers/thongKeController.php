<?php

namespace App\Http\Controllers;

use App\Comment;
use App\LoaiTin;
use App\TheLoai;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\ThongKe;
use App\TinTuc;
use App\User;

class thongKeController extends Controller
{
    public function getDanhSach(){
        $tintuc = TinTuc::orderBy('SoLuotXem','DESC')->get();
        return view('admin.thongke.danhsach',compact('tintuc'));
    }

    public function getDanhSachCmt(){
        $tintuc = TinTuc::all();
        return view('admin.thongke.danhsachcmt',compact('tintuc'));
    }

}
