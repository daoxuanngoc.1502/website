<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TheLoai;
use App\LoaiTin;
use App\Providers\RouteServiceProvider;
use App\TinTuc;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\URL;

class PagesController extends Controller
{

    function __construct()
    {
        $theloai = TheLoai::all();
        $loaitin = Loaitin::all();
        $tintuc = TinTuc::all();
        $user = User::all();
        view()->share('theloai',$theloai);
        view()->share('loaitin',$loaitin);
        view()->share('tintuc',$tintuc);
        view()->share('tintuc',$user);
    }

    function trangchu(Request $request){
        $theloai2 = TheLoai::where('id',1)->with(['tintuc'=>function($query){
            $query->where('Status',1)->limit(3);
        }])->first();
        $docNhieuNhat = TinTuc::orderBy('SoLuotXem','DESC')->where('Status',1)->limit(7)->get();
        $tieudiem = TinTuc::where('SoLuotXem','>',1)->where('Status',1)->limit(6)->get();
        return view('pages.trangchu',compact('theloai2','docNhieuNhat','tieudiem'));
    }

    function loaitin(Request $request,$id){
        $loaitin = LoaiTin::find($id);
        $dsLoaiTin = LoaiTin::limit(6)->get();
        $tintuc3 = TinTuc::orderBy('created_at','DESC')->where('idLoaiTin',$id)->limit(6)->get();
        $tintuc = TinTuc::where('idLoaiTin',$id)->where('Status',1)->limit(5)->get();
        $tintuc2 = TinTuc::where('idLoaiTin',$id)->where('Status',1)->paginate(6);

        return view('pages.loaitin',compact('loaitin','tintuc','dsLoaiTin','tintuc2','tintuc3'));
    }

    function tintuc(Request $request ,$id){
        if(Auth::check(Auth::user())){
            if(!($request->session()->has(Auth::user()->id.'-'.$id))) {
                $request->session()->put(Auth::user()->id.'-'.$id, Carbon::now()->addMinutes(1));
                TinTuc::where('id', $id)->increment('SoLuotXem');
            }
            if($request->session()->has(Auth::user()->id.'-'.$id)){
                if(Carbon::now() > $request->session()->get(Auth::user()->id.'-'.$id)){
                    $request->session()->forget(Auth::user()->id.'-'.$id);
                }
                $tintuc = TinTuc::find($id);
                $tinnoibat = TinTuc::where('NoiBat',1)->where('Status',1)->take(5)->get();
            }
            return view('pages.tintuc',compact('tintuc','tinnoibat'));
        }
        else{
            if(!($request->session()->has('Key-'.$id))) {
                $request->session()->put('Key-'.$id, Carbon::now()->addMinutes(1));
                TinTuc::where('id', $id)->increment('SoLuotXem');
            }
            if($request->session()->has('Key-'.$id)){
                if(Carbon::now() > $request->session()->get('Key-'.$id)){
                    $request->session()->forget('Key-'.$id);
                }
                $tintuc = TinTuc::find($id);
                $tinnoibat = TinTuc::where('NoiBat',1)->where('Status',1)->take(5)->get();
            }
            return view('pages.tintuc',compact('tintuc','tinnoibat'));
        }
    }

    function postTimKiem(Request $request){
        $tukhoa = $request->tukhoa;
        $tintuc = TinTuc::where('TieuDe','like',"%$tukhoa%")
        ->orwhere('TomTat','like',"%$tukhoa%")
        ->orwhere('NoiDung','like',"%$tukhoa%")
        ->get();

        return view('pages.timkiem',compact('tintuc','tukhoa'));
    }

    function getLogin(){
        return view('pages.login');
    }

    function postLogin(Request $request){
        $this->validate($request,[
            'password'=>'min:3|max:50',
        ],[
            'password.min'=>'Mật khẩu phải có ít nhất 3 ký tự',
            'password.max'=>'Mật khẩu tối đa 50 ký tự',
        ]);
        $data = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        if (Auth::attempt($data)) {
            return redirect(session('getUrl'));
        }else{
            return redirect('login')->with('thongbao','Sai tài khoản hoặc mật khẩu');
        }
    }

    function getDangXuat(){
        Auth::logout();
        return redirect(session('getUrl'));
    }

    function getDangKy(){
        return view('pages.dangky');
    }

    function postDangKy(Request $request){
        $this->validate($request,[
            'name'=>'required|min:3',
            'email'=>'required|email|unique:users,email',
            'password'=>'required|min:3|max:50',
            'passwordAgain'=>'required|same:password'
        ],[
            'name.required'=>'Bạn chưa nhập tên',
            'name.min'=>'Tên người dùng phải có ít nhất 3 ký tự',
            'email.required'=>'Bạn chưa nhập email',
            'email.unique'=>'Email đã tồn tại',
            'password.required'=>'Bạn chưa nhập mật khẩu',
            'password.min'=>'Mật khẩu phải có ít nhất 3 ký tự',
            'password.max'=>'Mật khẩu tối đa 50 ký tự',
            'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
            'passwordAgain.same'=>'Mật khẩu không khớp'
        ]);
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->provider = 'Client';
        $user->quyen = 0;
        $user->save();
        return redirect('login')->with('thongbao','Success!!');

    }
    // Login FB + GG
    protected $redirectTo = RouteServiceProvider::HOME;

    public function redirectToProvider($social)
    {
        return Socialite::driver($social)->redirect();
    }

    public function handleProviderCallback($social)
    {
        $user = Socialite::driver($social)->user();
        $user = User::firstOrCreate([
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'provider'=> $social,
            'provider_id' => $user->getId(),
            'quyen' => 0
        ]);
        Auth::Login($user,true);
        return redirect(session('getUrl'));
    }
}
