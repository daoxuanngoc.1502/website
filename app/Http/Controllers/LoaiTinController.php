<?php

namespace App\Http\Controllers;

use App\LoaiTin;
use App\TheLoai;
use Illuminate\Http\Request;

class LoaiTinController extends Controller
{
    public function getDanhSach(){
        $loaitin = LoaiTin::all();
        return view('admin.loaitin.danhsach',compact('loaitin'));
    }

    public function getThem(){
        $theloai= TheLoai::all();
        return view('admin.loaitin.them',compact('theloai'));
    }

    public function postThem(Request $request){
        $this->validate($request,[
            'Ten'=>'required|min:3|max:100'
        ],[
            'Ten'=>'Bạn chưa nhập tên thể loại',
            'Ten.min'=>'Tên thể loại phải có độ dài từ 3 -> 100 ký tự',
            'Ten.max'=>'Tên thể loại phải có độ dài từ 3 -> 100 ký tự'
        ]);

        $loaitin= new loaiTin;
        $loaitin->Ten = $request->Ten;
        $loaitin->idTheLoai = $request->TheLoai;
        $loaitin->TenKhongDau = changeTitle($request->Ten);
        
        $loaitin->save();
        return redirect('admin/loaitin/them')->with('thongbao','Success');
    }


    public function getSua($id){
        $theloai = TheLoai::all();
        $loaitin=loaitin::find($id);

        return view('admin.loaitin.sua',compact('loaitin','theloai'));
    }

    public function postSua(Request $request,$id){
        $loaitin=loaitin::find($id);
        $this->validate($request,[
            'Ten'=>'required|unique:loaitin,Ten|min:3|max:100'
        ],[
            'Ten'=>'Bạn chưa nhập tên thể loại',
            'Ten'=>'Tên thể loại đã tồn tại',
            'Ten'=>'Bạn chưa nhập tên thể loại',
            'Ten.min'=>'Tên thể loại phải có độ dài từ 3 -> 100 ký tự',
            'Ten.max'=>'Tên thể loại phải có độ dài từ 3 -> 100 ký tự'
        ]);

        $loaitin->Ten = $request->Ten;
        $loaitin->idTheLoai = $request->TheLoai;
        $loaitin->TenKhongDau = changeTitle($request->Ten);

        $loaitin->save();
        return redirect('admin/loaitin/sua/'.$id)->with('thongbao','Success');
    }

    public function getXoa($id){
        $loaitin= loaitin::find($id);
        $loaitin->delete();

        return redirect('admin/loaitin/danhsach')->with('thongbao','Success');
    }
}
