<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i = 1; $i <= 10;$i++)
        {
        	DB::table('Users')->insert( 
	        	[
	        		'name' => 'User_'.$i,
	            	'email' => 'user_'.$i.'@mymail.com',
	            	'password' => bcrypt('123456'),
	            	'quyen'=> 1,
	            	'created_at' => new DateTime(),
	        	]
        	);
        }

        // $data = [
        //     [
        //         'email' => 'lxc150896@gmail.com',
        //         'password' => bcrypt('12345'),
        //         'level' => 1,
        //     ],
        //     [
        //         'email' => 'lxc@gmail.com',
        //         'password' => bcrypt('12345'),
        //         'level' => 1,

        //     ],
        //     [
        //         'email' => 'admin@gmail.com',
        //         'password' => bcrypt('12345'),
        //         'level' => 1,
        //     ],
        //     [
        //         'email' => 'bangsh96lc@gmail.com',
        //         'password' => bcrypt('mabuucoi'),
        //         'level' => 1,
        //     ],
        // ];
        // DB::table('Users')->insert($data);
        
    }

}
