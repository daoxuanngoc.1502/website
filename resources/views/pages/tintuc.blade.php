    <script>
        window.fbAsyncInit = function() {
        FB.init({
            appId      : env('FACEBOOK_CLIENT_ID'),
            cookie     : true,
            xfbml      : true,
            version    : 'v7.0'
        });

        FB.AppEvents.logPageView();

        };

        (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=324083185263486&autoLogAppEvents=1"
        nonce="7VZKwUrg">
    </script>

   @extends('layout.index')

    @section('css')
        <style>
            .comment-face {
                margin: 20px;
                font-family: Helvetica, Arial, sans-serif;
            }

                    .pull-right {
                float: right;
            }

            .pu        ll-left {
                float: left;
            }

            #fbcomm        ent {
                background: #fff;
                border: 1px solid #dddfe2;
                border-radius: 3px;
                color: #4b4f56;
                padding: 50px;
            }

            .header_comment         {
                font-size: 14px;
                overflow: hidden;
                border-bottom: 1px solid #e9ebee;
                line-height: 25px;
                margin-bottom: 24px;
                padding: 10px 0;
            }

            .sort_title {
                        color: #4b4f56;
            }

            .sort_by {
                backg        round-color: #f5f6f7;
                color: #4b4f56;
                line-height: 22px;
                cursor: pointer;
                vertical-align: top;
                font-size: 12px;
                font-weight: bold;
                vertical-align: middle;
                padding: 4px;
                justify-content: center;
                border-radius: 2px;
                border: 1px solid #ccd0d5;
            }

            .count_comment {
                font-weight: 6        00;
            }

            .body_comment {
                padding: 0 8px;
                        font-size: 14px;
                display: block;
                line-height: 25px;
                word-break: break-word;
            }

            .avatar_comment {
                display: block;
            }

            .avatar_comment img {
                height: 48px;
                        width: 48px;
            }

            .box_comment {
                display: block;
                position:         relative;
                line-height: 1.358;
                word-break: break-word;
                border: 1px solid #d3d6db;
                word-wrap: break-word;
                background: #fff;
                box-sizing: border-box;
                cursor: text;
                font-family: Helvetica, Arial, sans-serif;
                font-size: 16px;
                padding: 0;
            }

            .box_comment textarea {
                min-height: 40px;
                padding: 12px         8px;
                width: 100%;
                border: none;
                resize: none;
            }

            .box_comment textarea:focus {
                outline: none !important;
            }

                    .box_comment .box_post {
                border-top: 1px solid #d3d6db;
                background:         #f5f6f7;
                padding: 8px;
                display: block;
                overflow: hidden;
            }

            .box_comment label {
                display: inline-block;
                vertical-align: middle;
                        font-size: 11px;
                color: #90949c;
                line-height: 22px;
            }

            .box_comment button {
                margin-left: 8px;
                background-color: #4267b2;
                bord        er: 1px solid #4267b2;
                color: #fff;
                text-decoration: none;
                line-height: 22px;
                border-radius: 2px;
                font-size: 14px;
                font-weight: bold;
                position: relative;
                text-align: center;
            }

            .box_comment button:hover {
                background-color: #29487d;
                border-color: #29487d;
            }

                    .box_comment .cancel {
                margin-left: 8px;
                background-color: #f5f6f7;
                color: #4b4f56;
                        text-decoration: none;
                line-height: 22px;
                border-radius: 2px;
                font-size: 14px;
                font-weight: bold;
                position: relative;
                text-align: center;
                border-color: #ccd0d5;
            }

            .box_comment .cancel:hover {
                background-color: #d0d0d0;
                border-color: #ccd0d5;
            }

            .box_comment img {
                height: 16px;
                width: 16px;
            }

            .box_result {
                margin-top: 24px;
            }

                    .box_result .result_comment h4 {
                font-weight: 600;
                white-space:         nowrap;
                color: #365899;
                cursor: pointer;
                        text-decoration: none;
                font-size: 14px;
                line-height: 1.358;
                margin: 0;
            }

            .box_result .result_comment {
                display: block;
                overflow: hidden;
                padding: 0;
            }

            .child_replay {
                border        -left: 1px dotted #d3d6db;
                margin-top: 12px;
                list-style: none;
                padding: 0 0 0 8px
            }

                    .reply_comment {
                margin: 12px 0;
            }

            .box_result .result_comment p {
                margin: 4px 0;
                text-align: justify;
            }

            .bo        x_result .result_comment .tools_comment {
                font-size: 12p        x;
                line-height: 1.358;
            }

            .box_result .result_comment .tools_comment a {
                col        or: #4267b2;
                cursor: pointer;
                text-decoration: none;
            }

            .box_result .result_comment .tools        _comment span {
                color: #90949c;
            }

            .body_comment .show_more {
                background: #3578e5;
                border: none;
                box        -sizing: border-box;
                color: #fff;
                font-size: 14px;
                margin-top: 24        px;
                padding: 12px;
                text-shadow: none;
                width: 100%;
                font-weight: bold;
                position: relative;
                text-align: center;
                vertical-align: middle;
                border-radius: 2px;
            }
            .video,
            iframe{
                width: 750px;
                height: 500px;
                margin-bottom: 20px;
            }
        </style>
    @endsection

    @section('content')
    <section class="">
        <div class="container">
            <a href="trangchu" style="color: #c3c3c3;">Trang Chủ > </a>
            <a href="loaitin" style="color: #c3c3c3;">{{ $tintuc->loaitin->theloai->Ten }} </a>
            <span class="" style="color: #c3c3c3;" >  >  {{ $tintuc->loaitin->Ten }}  </span>
            <span class="" style="color: #c3c3c3;" >  >  {{ $tintuc->TieuDe }}  </span>
            <p class="name-product p-loaitin"
            style="display:-webkit-box;-webkit-line-clamp: 2;-webkit-box-orient: vertical;overflow: hidden">
                {{ $tintuc->TieuDe }} <br>
                <span style="font-size: 15px;">
                    <img src="image/eye.png" width="18px" alt=""> {{ $tintuc->SoLuotXem }}
                    <img src="image/comment.png" width="18px" alt=""> {{ count($tintuc->comment) }}
                </span>
            </p>

        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <img src="upload/tintuc/{{ $tintuc->Hinh }} ? upload/tintuc/{{ $tintuc->Hinh }} : $tintuc->Hinh"
                        alt="" class="img-responsive" style="width:750px;height: 500px;">
                    <p class="tomtat">{{ $tintuc->TomTat }}</p>
                    @if (!empty($tintuc->Video))
                        <p class="video">
                            {!! $tintuc->Video !!}
                        </p>
                    @endif
                    <p class="noidung">{!! $tintuc->NoiDung !!}</p>
                    <span style="font-weight: 700;">Ký Danh:</span> {{ $tintuc->KyDanh }}
                    <hr>
                </div>
                <div class="col-md-4" >
                    <p style="line-height: 30px;background-color: red;color: #fff;
                    font-size: 16px;padding-left: 10px;width: 100px;">Bài viết mới</p>
                    <hr style="margin-top: 0px;border: 0;border-top: 1px solid red;">
                    @foreach ($tinnoibat as $tt)
                    <div style="margin-bottom: 20px;">
                        <div>
                            <a href="tintuc/{{ $tt->id }}/{{ $tt->TieuDeKhongDau }}.html">
                                {{ $tt->TomTat }}
                            </a>
                            <hr class="line-gray">
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

    </section>
    @if (Auth::check(Auth::user()))
        @if (Auth::user()->provider == 'facebook')
            <div class="container">
                <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator"
                    data-numposts="5" data-width="">
                </div>
            </div>
        @endif
    @endif
    <section class="comment-face">
        <?php
            if(Auth::check())
            {
                $nguoidung = Auth::user();
            }
        ?>
        <div class="container">
            <div class="col-md-12" id="fbcomment">
                <div class="header_comment">
                    <div class="row">
                        {{--  <div class="col-md-6 text-left">
                                <span class="count_comment">264235 Comments</span>
                        </div>
                        <div class="col-md-6 text-right">
                            <span class="sort_title">Sort by</span>
                            <select class="sort_by">
                            <option>Top</option>
                            <option>Newest</option>
                            <option>Oldest</option>
                        </select>
                        </div>  --}}
                    </div>
                </div>
                <div class="body_comment">
                    @if(isset($nguoidung))
                        <form action="comment/{{ $tintuc->id }}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <div class="row">
                                <div class="avatar_comment col-md-1">
                                    <img src="https://static.xx.fbcdn.net/rsrc.php/v1/yi/r/odA9sNLrE86.jpg" alt="avatar" />
                                </div>
                                <div class="box_comment col-md-11">
                                    <textarea class="commentar" placeholder="Add a comment..." name="NoiDung"></textarea>
                                    <div class="box_post">

                                        <div class="pull-right">
                                            <span>
                                        <img src="https://static.xx.fbcdn.net/rsrc.php/v1/yi/r/odA9sNLrE86.jpg" alt="avatar" />
                                        <i class="fa fa-caret-down"></i>
                                    </span>
                                            <button onclick="submit_comment()" type="submit">Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    @endif
                    <div class="row">
                        @foreach ($tintuc->comment as $cm)
                        <ul id="list_comment" class="col-md-12">
                            <!-- Start List Comment 1 -->
                            <li class="box_result row">
                                <div class="avatar_comment col-md-1">
                                    <img src="https://static.xx.fbcdn.net/rsrc.php/v1/yi/r/odA9sNLrE86.jpg" alt="avatar" />
                                </div>
                                <div class="result_comment col-md-11">
                                    <h4>{{ $cm->user->name }}</h4>
                                    <p>{{ $cm->NoiDung }}</p>
                                    <p>{{ $cm->created_at }}</p>
                                    <div class="tools_comment">

                                        {{--  <a class="like" href="#">Like</a>
                                        <span aria-hidden="true"> · </span>
                                        <a class="replay" href="#">Reply</a>
                                        <span aria-hidden="true"> · </span>
                                        <i class="fa fa-thumbs-o-up"></i> <span class="count">1</span>
                                        <span aria-hidden="true"> · </span>
                                        <span>26m</span>  --}}
                                    </div>

                                </div>
                            </li>
                        </ul>
                    </form>
                        @endforeach
                        {{--  <button class="show_more" type="button">Load 10 more comments</button>  --}}
                    </div>
                </div>

            </div>
        </div>


    </section>

    @section('script')
    <script>
        function submit_comment() {
            var comment = $('.commentar').val();
            el = document.createEle ment('li');
            el.className = "box_result row";
            el.innerHTML =
                '<div class=\"avatar_comment col-md-1\">' +
                '<img src=\"https://static.xx.fbcdn.net           /rsrc.php/v1/yi/r/odA9sNLrE86.jpg\" alt=\"avatar\"/>' +
                '</div>' +
                '<div class=\"result_comment col-md-11\">' +
                '<h4>Anonimous</h4>' +
                '<p>' + comment + '</p>' +
                '<div class=\"tools_comment\">' +
                '<a class=\"like\" href=\"#\">Like</a><span aria-hidden=\"true\"> · </span>' +
                '<i class=\"fa fa-thumbs-o-up\"></i> <span class=\"count\">0</span>' +
                '<span aria-hidden=\"true\"> · </span>' +
                '<a class=\"replay\" href=\"#\">Reply</a><span aria-hidden=\"true\"> · </span>' +
                '<span>1m</span>' +
                '</div>' +
                '<ul class="child_replay"></ul>' +
                '</div>';
            document.getElementById('list_comment').prepend(el);
            $('.commentar').val('');
        }

        $(document).ready(function() {
            $('#list_comment').on('click', '.like', function(e) {
                $current = $(this);
                var x = $current.closest('div').find('.like').text().trim();
                var y = parseInt($current.closest('div').find('.count').text().trim());

                if (x === "Like") {
                    $current.closest('div').find('.like').text('Unlike')    ;
                    $current.closest('div').find('.count').text(y + 1);
                } else if (x === "Unlike") {
                    $current.closest('div').find('.like').text('Like');
                    $current.closest('div').find('.count').text(y - 1);
                } else {
                    var repla    y = $current.closest('div').find('.like').text('Like');
                    $current.closest('div').find('.count').text(y - 1);
                }
            });

            $('#list_comment').on('click', '.replay', function(e) {
                cancel_reply();
                $current = $(this);
                el = document.createElement('li');
                el.className = "box_reply row";
                el.innerHTML =
                    '<div class=\"col-md-12 reply_comment\">' +
                    '<div class=\"row\">' +
                    '<div class=\"avatar_comment col-md-1\">' +
                        '<img src=\"https://static.xx.fbcdn.net/rsrc.php/v1/yi/r/odA9sNLrE86.jpg\" alt=\"avatar\"/>' +
                    '</div>' +
                    '<div class=\"box_comment col-md-10\">' +
                    '<textarea class=\"comment_replay\" placeholder=\"Add a comment...\"></textarea>' +
                    '<div class=\"box_post\">' +
                    '<div class=\"pull-right\">' +
                    '<span>' +
                    '<img src=\"https://static.xx.fbcdn.net/rsrc.php/v1/yi/r/odA9sNLrE86.jpg\" alt=\"avatar\" />' +
                    '<i class=\"fa fa-caret-down\"></i>' +
                    '</span>' +
                    '<button class=\"cancel\" onclick=\"cancel_reply()\" type=\"button\">Cancel</button>' +
                    '<button onclick=\"submit_reply()\" type=\"button\" value=\"1\">Reply</button>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                $current.closest('li').find('.child_replay').prepend(el);
            });
        });

        function submit_reply() {
            var comment_replay = $('.comment_replay').val();
            el = document.createElement('li');
            el.className = "box_reply row";
            el.innerHTML =
                '<div class=\"avatar_comment col-md-1\">' +
                '<img src=\"https://static.xx.fbcdn.net/rsrc.php/v1/yi/r/odA9sNLrE86.jpg\" alt=\"avatar\"/>' +
                '</div>' +
                '<div class=\"result_comment col-md-11\">' +
                '<h4>Anonimous</h4>' +
                '<p>' + comment_replay + '</p>' +
                '<div class=\"tools_comment\">' +
                '<a class=\"like\" href=\"#\">Like</a><span aria-hidden=\"true\"> · </span>' +
                '<i class=\"fa fa-thumbs-o-up\"></i> <span class=\"count\">0</span>' +
                '<span aria-hidden=\"true\"> · </span>' +
                '<a class=\"replay\" href=\"#\">Reply</a><span aria-hidden=\"true\"> · </span>' +
                '<span>1m</span>' +
                '</div>' +
                '<ul class="child_replay"></ul>' +
                '</div>';
            $current.closest('li').find('.child_replay').prepend(el);
            $('.comment_replay').val('');
            cancel_reply();
        }

        function cancel_reply() {
            $('.reply_comment').remove();
        }
    </script>
    @endsection

    @endsection
