@extends('layout.index')
@section('css')
    <style>
        a:hover{
            transform: scale(1) !important;
        }
    </style>
@endsection
@section('content')
    <input type="text" name="getURL" value="{{ session()->get('URL') }}" id="demo" hidden>
    <div class="container">
        <p style="font-size: 25px;font-weight: 700;color: red;margin-bottom: 20px;">{{ $loaitin->Ten }}</p>
    </div>
    <section >
        <div class="container-fluid">
            <div class="row">
                <?php
                    $data = $tintuc->shift();
                ?>
                <div class="col-md-6">
                    <div class="img-main">
                        <a href="tintuc/{{ $data['id'] }}/{{ $data['TieuDeKhongDau'] }}.html">
                            <img src="upload/tintuc/{{ $data['Hinh'] }}" alt="Không tải được ảnh"
                            class="img-930-535 img-responsive imgHover" width="100%">
                        </a>
                        <a href="tintuc/{{ $data['id'] }}/{{ $data['TieuDeKhongDau'] }}.html"class="p-main"
                            style="line-height: 24px; display:-webkit-box;-webkit-line-clamp: 2;
                                -webkit-box-orient: vertical;overflow: hidden;">
                            {{ $data['TomTat'] }}
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    @foreach ($tintuc as $tt)
                    <div class="col-md-6">
                        <div class="img-main">
                            <a href="tintuc/{{ $tt->id }}/{{ $tt->TieuDeKhongDau }}.html">
                                <img src="upload/tintuc/{{ $tt->Hinh }}" alt="" class="img-465-262 img-responsive imgHover">
                            </a>
                            <a href="tintuc/{{ $tt->id }}/{{ $tt->TieuDeKhongDau }}.html" class="p-sub"
                                style="display:-webkit-box;-webkit-line-clamp: 2;-webkit-box-orient: vertical;overflow: hidden;">
                                {{ $tt->TomTat }}
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <section class="mar-50">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <p></p>
                    <div class="row">
                        @foreach ($tintuc2 as $tt2)
                        <div class="col-md-6">
                            <a href="tintuc/{{ $tt2->id }}/{{ $tt2->TieuDeKhongDau }}.html">
                                <img src="upload/tintuc/{{ $tt2->Hinh }}" alt="" class="img-240-160 img-responsive imgHover">
                            </a>
                            <a href="tintuc/{{ $tt2->id }}/{{ $tt2->TieuDeKhongDau }}.html" class="p-text-lt " style="display:-webkit-box;-webkit-line-clamp: 2;-webkit-box-orient: vertical;overflow: hidden">
                            {{ $tt2->TomTat }}
                        </a>
                        </div>
                        @endforeach
                    </div>
                    </div>
                    <div class="col-md-4">
                        <p style="line-height: 30px;background-color: red;color: #fff;
                    font-size: 16px;padding-left: 10px;width: 100px;">Bài viết mới</p>
                        <hr style="margin-top: 0px;border: 0;border-top: 1px solid red;">
                    @foreach ($tintuc3 as $tt)
                        <div style="margin-bottom: 20px;">
                            <div>
                            <a href="tintuc/{{ $tt->id }}/{{ $tt->TieuDeKhongDau }}.html">
                                {{ $tt->TomTat }}
                            </a>
                            <hr class="line-gray">
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            {{ $tintuc2->links() }}
        </div>
    </section>
@endsection
