@extends('layout.index')

@section('css')
    <style>
        .text-hidden {
            display: -webkit-box;
            -webkit-line-clamp: 2;
            -webkit-box-orient: vertical;
            overflow: hidden;
            font-weight: 700;
            padding-top: 10px;
        }
        a:hover{
            transform: none !important;
        }
    </style>
@endsection

@section('content')
<section class="thoisu" >
    <div class="container">
        <p class="name">
            {{ $theloai2->Ten }}
            @foreach ($theloai2->loaitin as $lt)
                <a href="loaitin/{{ $lt->id }}/{{ $lt->TenKhongDau }}.html" style="font-size: 15px;font-weight: 400;">
                    <span > / {{ $lt->Ten }} </span>
                </a>
            @endforeach
            <hr class="line-eee">
        </p>
        <div class="row">
                @foreach ($theloai2->tintuc as $item2)
                    <div class="col-md-4">
                        @if($item2->Hinh)
                            <a href="tintuc/{{ $item2->id }}/{{ $item2->TieuDeKhongDau }}.html">
                                <img src="upload/tintuc/{{ $item2->Hinh }}" alt="" class="img-thoisu img-responsive imgHover">
                            </a>
                        @endif
                        <a href="tintuc/{{ $item2->id }}/{{ $item2->TieuDeKhongDau }}.html"
                            class="text-hidden" style="margin-bottom: 10px;font-size: 17px;
                            line-height: 24px; display:-webkit-box;-webkit-line-clamp: 1;
                            -webkit-box-orient: vertical;overflow: hidden;">
                            {{ $item2->TieuDe ? $item2->TieuDe : '' }}
                        </a>
                        <p>
                            {{ $item2->updated_at->toFormattedDateString() }}
                            <span style="font-size: 15px;float:right">
                                <img src="image/eye.png" width="18px" alt=""> {{ $item2->SoLuotXem }}
                            </span>
                        </p>
                    </div>
                @endforeach
        </div>
    </div>
</section>

<section class="tieudiem" style="margin-top: 40px;">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <p class="name">
                    tiêu điểm
                </p>
                <hr class="line-eee">
                <div class="row">
                    <?php
                        $tinTieuDiem = $tieudiem->shift();
                        $tinTieuDiem1 = $tieudiem->shift();
                    ?>
                    <div class="col-md-6">
                        <a href="tintuc/{{ $tinTieuDiem['id'] }}/{{ $tinTieuDiem['TieuDeKhongDau'] }}.html">
                            <img src="upload/tintuc/{{  $tinTieuDiem['Hinh'] }}" alt="" class="img-responsive imgHover" style="width: 100%;">
                        </a>
                        <a class="text-hidden" style="float: left;padding-bottom: 10px;font-size: 17px;" href="tintuc/{{ $tinTieuDiem['id'] }}/{{ $tinTieuDiem['TieuDeKhongDau'] }}.html">{{  $tinTieuDiem['TieuDe'] }}</a>
                        <span style="float: left;">{{  $tinTieuDiem['TomTat'] }}
                            <span style="font-size: 15px;float: right;">
                                <img src="image/eye.png" width="18px" alt=""> {{ $tinTieuDiem['SoLuotXem'] }}
                            </span>
                        </span>

                    </div>
                    <div class="col-md-6">
                        <a href="tintuc/{{ $tinTieuDiem1['id'] }}/{{ $tinTieuDiem1['TieuDeKhongDau'] }}.html">
                            <img src="upload/tintuc/{{  $tinTieuDiem1['Hinh'] }}" alt="" class="img-responsive imgHover" style="width: 100%;">
                        </a>
                        <a class="text-hidden" style="float: left;padding-bottom: 10px;font-size: 17px;" href="tintuc/{{ $tinTieuDiem1['id'] }}/{{ $tinTieuDiem1['TieuDeKhongDau'] }}.html">{{  $tinTieuDiem1['TieuDe'] }}</a>
                        <span style="float: left;">
                            {{  $tinTieuDiem1['TomTat'] }}
                            <span style="font-size: 15px;float: right;">
                                <img src="image/eye.png" width="18px" alt=""> {{ $tinTieuDiem['SoLuotXem'] }}
                            </span>
                        </span>

                    </div>

                </div>
                <div class="row" style="margin-bottom: 40px;">
                    @foreach ($tieudiem as $td)
                    <div class="col-md-6" style="margin:15px 0">
                        <a href="tintuc/{{ $td->id }}/{{ $item2->TieuDeKhongDau }}.html">
                            <img src="upload/tintuc/{{ $td->Hinh }}" alt=""
                            style="float: left;padding-right: 30px;width: 200px;height: 160px;" class="imgHover">
                        </a>
                        <a href="tintuc/{{ $td->id }}/{{ $item2->TieuDeKhongDau }}.html" class="texiddet-hn">{{ $td->TieuDe }}</a>
                        <p style="padding-top: 75px">{{ $td->updated_at->toFormattedDateString() }}
                            <span style="font-size: 15px;float: right;">
                                <img src="image/eye.png" width="18px" alt=""> {{ $item2->SoLuotXem }}
                            </span>
                        </p>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-4">
                <p class="name">
                    đọc nhiều nhất
                </p>
                <hr class="line-eee">
                @foreach ($docNhieuNhat as $tt)
                <div style="margin-bottom: 20px;">
                    <div>
                        <a href="tintuc/{{ $tt->id }}/{{ $tt->TieuDeKhongDau }}.html" style="font-weight: 700;">
                            {{ $tt->TomTat }}
                        </a>
                        <span style="font-size: 15px;float: right;">
                            <img src="image/eye.png" width="18px" alt=""> {{ $tt->SoLuotXem }}
                        </span>
                        <hr class="line-gray">
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@foreach ($theloai as $key => $tl)
    @if($key % 2 == 0 && $tl->id != 1 && count($tl->loaitin) > 0)
        <section class="xahoi" style="margin-top: 40px;">
            <div class="container">
                <p class="name">
                    {{ $tl->Ten }}
                    @foreach ($tl->loaitin as $lt)
                        <a href="loaitin/{{ $lt->id }}/{{ $lt->TenKhongDau }}.html"
                            style="font-size: 15px;font-weight: 400;">
                            <span > / {{ $lt->Ten }} </span>

                        </a>
                    @endforeach
                </p>
                <hr class="line-eee">
                <div class="row">
                    <?php
                    $data = App\TheLoai::where('id',$tl->id)->with(['tintuc'=>function($query){
                        $query->limit(6);
                        }])->first();
                        $tintuc1 = $data->tintuc->first();
                    ?>
                        <div class="col-md-8">
                            <a href="intuc/{{ $tintuc1['id'] }}/{{ $tintuc1['TieuDeKhongDau'] }}.html">
                                <img src="upload/tintuc/{{ $tintuc1['Hinh'] }}" alt="" class="img-responsive imgHover" style="width: 765px;height: 550px;">
                            </a>
                            <a style="float: left;text-transform: capitalize;font-weight: 700;font-size: 20px;line-height: 40px" href="tintuc/{{ $tintuc1['id'] }}/{{ $tintuc1['TieuDeKhongDau'] }}.html">{{ $tintuc1['TieuDe'] }}</a>
                            <span style="float: left;">{{ $tintuc1['TomTat'] }}
                                <span style="font-size: 15px;float: right;">
                                    <img src="image/eye.png" width="18px" alt=""> {{ $item2->SoLuotXem }}
                                </span>
                            </span>
                        </div>
                        <div class="col-md-4" >
                            @foreach ($data->tintuc as $item)
                            @if($item['id'] != $tintuc1['id'])
                            <div style="margin-bottom: 20px;">
                                <a href="tintuc/{{ $item['id'] }}/{{ $item['TieuDeKhongDau'] }}.html">
                                    <img src="upload/tintuc/{{ $item['Hinh'] }}" style="width:90px;height:90px;" class="img-responsive imgHover">
                                </a>
                                <div style="width: 65%;float: right;margin-top: -90px;">
                                    <a href="tintuc/{{ $item['id'] }}/{{ $item['TieuDeKhongDau'] }}.html" style="float: left;font-weight: 700;">
                                        {{ $item['TieuDe'] }}
                                    </a>
                                </div>
                                <p style="float: right;position: relative;top: -26px;">{{ $item['updated_at']->toFormattedDateString() }}
                                    <span style="font-size: 15px;float: right;margin-left: 10px">
                                        <img src="image/eye.png" width="18px" alt=""> {{ $item['SoLuotXem'] }}
                                    </span>
                                </p>
                            </div>
                            @endif
                            @endforeach
                        </div>
                </div>
            </div>
        </section>
    @endif
    @if($key % 2 != 0 && $tl->id != 8)
        <section class="thegioi" style="margin-top: 40px;">
            <div class="container">
                <p class="name">
                    {{ $tl->Ten }}
                    @foreach ($tl->loaitin as $lt)
                        <a href="loaitin/{{ $lt->id }}/{{ $lt->TenKhongDau }}.html" style="font-size: 15px;font-weight: 400;">
                            <span > / {{ $lt->Ten }} </span>
                        </a>
                    @endforeach
                </p>
                <hr class="line-eee">
                <div class="row">
                    <?php
                    $data2 = App\TheLoai::where('id',$tl->id)->with(['tintuc'=>function($query){
                            $query->limit(7);
                        }])->first();
                    $tintuc2 = $data2->tintuc->first();
                    ?>
                    @foreach ($data2->tintuc as $item2)
                        @if($item2['id'] != $tintuc2['id'])
                            <div class="col-md-4" style="margin: 20px 0">
                                <a href="tintuc/{{ $item2['id'] }}/{{ $item2['TieuDeKhongDau'] }}.html" class="text-hidden">
                                    <img src="upload/tintuc/{{ $item2['Hinh'] }}" alt="" class="img-thoisu img-responsive imgHover">
                                    {{--  <span style="font-size: 15px;float: right;margin-left: 10px">
                                        <img src="image/eye.png" width="18px" alt=""> {{ $item2['SoLuotXem'] }}
                                    </span>  --}}
                                </a>
                                <a href="tintuc/{{ $item2['id'] }}/{{ $item2['TieuDeKhongDau'] }}.html"
                                    class="text-hidden">{{ $item2['TieuDe'] }}

                                </a>
                                <p>
                                    <span>{{ $item2['created_at'] }}</span>
                                </p>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </section>
    @endif
@endforeach
@endsection
