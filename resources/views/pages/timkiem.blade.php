@extends('layout.index')

@section('css')
    <style>
        .text-hidden {
            display: -webkit-box;
            -webkit-line-clamp: 2;
            -webkit-box-orient: vertical;
            overflow: hidden;
            font-weight: 700;
            padding-top: 10px;
        }
        a:hover{
            transform: none !important;
        }
    </style>
@endsection

@section('content')
    <section>
        <div class="container">
            <p class="name">
                Tìm Kiếm: <span style="color: gray;font-size: 20px;font-weight: 700">{{ $tukhoa }}</span>
            </p>
            <div class="row">
                @foreach ($tintuc as $item2)
                    <div class="col-md-12">
                        <a href="tintuc/{{ $item2->id }}/{{ $item2->TieuDeKhongDau }}.html">
                            <img src="upload/tintuc/{{ $item2->Hinh }}" alt="" class="img-thoisu img-responsive imgHover"
                            style="float: left;padding-right: 20px;margin-bottom: 20px">
                        </a>
                        <a href="tintuc/{{ $item2->id }}/{{ $item2->TieuDeKhongDau }}.html"
                            class="text-hidden" style="margin-bottom: 10px;font-size: 17px;" style="transform: none">
                            {{ $item2->TieuDe ? $item2->TieuDe : '' }}
                        </a>
                        <p>
                            {{ $item2->TomTat }}
                        </p>
                    </div>
                @endforeach

            </div>
        </div>
    </section>
@endsection
