<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <base href="{{ asset('') }}">
    <meta name="author" content="ThemePunch" />
    <meta name="description" content="The Garden theme tempalte">
    <meta name="keywords" content="The Garden theme template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="icon" href="images/favicon.ico" type="image/gif" sizes="16x16"> -->
    <!--Icons fonts-->
    <link href="admin_asset/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="admin_asset/vendor/themify-icons/themify-icons.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i|Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

    <!--Styles-->
    <link href="admin_asset/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="admin_asset/vendor/animsition/dist/css/animsition.min.css" rel="stylesheet">
    <link href="admin_asset/vendor/animate.css/source/slide_fwd_center/slide_fwd_center.css" rel="stylesheet">
    <link href="admin_asset/vendor/owl-carousel/css/owl.carousel.css" rel="stylesheet">
    <link href="admin_asset/vendor/css-hamburgers/css/hamburgers.min.css" rel="stylesheet">
    <link href="admin_asset/vendor/slick/css/slick.css" rel="stylesheet">
    <link href="admin_asset/vendor/range_filter/css/jquery-ui.css" rel="stylesheet">
    <!-- Revolution -->
    <link rel="stylesheet" type="text/css" href="admin_asset/vendor/slider-revolution/css/settings.css">
    <link rel="stylesheet" type="text/css" href="admin_asset/vendor/slider-revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="admin_asset/vendor/slider-revolution/css/navigation.css">
    <!--Theme style-->
    <link href="admin_asset/css/main.css" rel="stylesheet">
    <link href="admin_asset/css/fonts.css" rel="stylesheet">
    <link href="admin_asset/css/style.css" rel="stylesheet">
    <link href="admin_asset/css/responsive.css" rel="stylesheet">
    
</head>

<body>
        @if (count($errors)>0)
            <div class="alert alert-danger">
                @foreach ($errors->all() as $er)
                    {{ $er }}<br>
                @endforeach
            </div>
        @endif

        @if (session('thongbao'))
             <div class="alert alert-success">
                    {{ session('thongbao') }}
            </div>
        @endif

    <div class="login-box">
        {{--  <div class="lb-header">
            <a href="admin/dangnhap" class="active" id="login-box-link">Login</a>
            <a href="admin/dangnhap" id="signup-box-link">Sign Up</a>
        </div>  --}}
        <div class="social-login">
            <p style="text-transform: uppercase; font-size: 25px;font-weight: 700;text-align: center"
            >Đăng Nhập</p>
            {{--  <a href="#">
                <i class="fa fa-facebook fa-lg"></i> Login in with facebook
            </a>
            <a href="#">
                <i class="fa fa-google-plus fa-lg"></i> log in with Google
            </a>  --}}
        </div>

        <form class="email-login" action="admin/dangnhap" method="POST" role="form" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="u-form-group">
                <input type="email" placeholder="Email" name="email" autofocus required />
            </div>
            <div class="u-form-group">
                <input type="password" placeholder="Password" name="password" required/>
            </div>
            <div class="u-form-group">
                <button type="submit">Log in</button>
            </div>
        </form>

        <form class="email-signup" action="admin/dangnhap" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="u-form-group">
                <input type="email" placeholder="Email" name="email" />
            </div>
            <div class="u-form-group">
                <input type="password" placeholder="Password" name="password"/>
            </div>
            <div class="u-form-group">
                <input type="password" placeholder="Confirm Password" name="passwordAgain" />
            </div>
            <div class="u-form-group">
                <button type="submit">Sign Up</button>
            </div>
        </form>
    </div>


    <script src="admin_asset/vendor/jquery/dist/jquery.min.js"></script>
    <script src="admin_asset/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="admin_asset/vendor/jquery_easing/jquery.easing.min.js"></script>
    <script src="admin_asset/vendor/owl-carousel/js/owl.carousel.js"></script>
    <script src="admin_asset/vendor/slick/js/slick.js"></script>
    <script src="admin_asset/vendor/isotope/js/isotope.js"></script>
    <script src="admin_asset/vendor/isotope/js/imagesloaded.pkgd.js"></script>
    <script src="admin_asset/vendor/range_filter/js/jquery-ui.js"></script>
    <script type="text/javascript" src="admin_asset/vendor/slider-revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="admin_asset/vendor/slider-revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="admin_asset/vendor/slider-revolution/js/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="admin_asset/vendor/slider-revolution/js/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="admin_asset/vendor/slider-revolution/js/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="admin_asset/vendor/slider-revolution/js/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="admin_asset/vendor/slider-revolution/js/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="admin_asset/vendor/slider-revolution/js/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="admin_asset/vendor/slider-revolution/js/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="admin_asset/vendor/slider-revolution/js/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="admin_asset/vendor/slider-revolution/js/revolution.extension.video.min.js"></script>
    <script src="admin_asset/script/main.js"></script>
</body>

</html>