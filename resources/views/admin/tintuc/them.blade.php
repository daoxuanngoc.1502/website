@extends('admin.layout.index')

@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tin Tức
                    <small>Thêm</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                @if (count($errors)>0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $er)
                            {{ $er }}<br>
                        @endforeach
                    </div>
                @endif

                @if (session('thongbao'))
                    <div class="alert alert-success">
                        {{ session('thongbao') }}
                    </div>
                @endif
                @if(Auth::user()->quyen == 1 or Auth::user()->quyen == 3)
                <form action="admin/tintuc/them" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <label>Thể Loại</label>
                        <select class="form-control" name="TheLoai" id="TheLoai">
                            @foreach ($theloai as $tl)
                            <option

                            value="{{ $tl->id }}">{{ $tl->Ten }}</option>
                            @endforeach

                        </select>
                    </div>
                    <div class="form-group"  >
                        <label>Loại Tin</label>
                        <select class="form-control" id="LoaiTin" name="LoaiTin">
                            @foreach ($loaitin as $lt)
                                <option value="{{ $lt->id }}">{{ $lt->Ten }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tiêu đề</label>
                        <input class="form-control" name="TieuDe" placeholder="Nhập tiêu đề" />
                    </div>

                    <div class="form-group">
                        <label>Tóm tắt</label>
                        <textarea id="demo" class="form-control " name="TomTat"></textarea>
                    </div>

                    <div class="form-group">
                        <label>Video</label>
                        <input class="form-control" name="Video" laceholder="Đọc VD-Nhung-Link-Video.docx để thêm video vào bài viết"/>
                    </div>

                    <div class="form-group">
                        <label>Nội Dung</label>
                        <textarea id="editor1" class="form-control ckeditor" name="NoiDung"></textarea>
                    </div>

                    <div class="form-group">
                        <label>Hình Ảnh</label>
                        <input class="form-control" name="Hinh" type="file" multiple/>
                    </div>

                    <div class="form-group">
                        <label>Nổi Bật</label>
                        <label class="radio-inline">
                            <input name="rdoStatus" value="0" checked="" type="radio" name="NoiBat">Không
                        </label>
                        <label class="radio-inline">
                            <input name="rdoStatus" value="1" type="radio" name="NoiBat">Có
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Ký Danh</label>
                        <input type="text" class="form-control " name="KyDanh"></input>
                    </div>
                    <button type="submit" class="btn btn-default">Thêm</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                <form>
                @endif
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>

@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $("#TheLoai").change(function(){
                var idTheLoai = $(this).val();
                $.get("admin/ajax/loaitin/"+idTheLoai,function(data){
                    $("#LoaiTin").html(data);
                });
            });
        });
    </script>
    <script>
        CKEDITOR.replace( 'editor1' );
    </script>

@endsection
