@extends('admin.layout.index')

@section('content')

<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tin Tức
                            <small>Danh Sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if (session('thongbao'))
                        <div class="alert alert-success">
                            {{ session('thongbao') }}
                        </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Tiêu đề</th>
                                <th>Tóm tắt</th>
                                <th>Thể loại</th>
                                <th>Loại Tin</th>
                                <th>Ký Danh</th>
                                <th>Status</th>
                                <th>Nổi bật</th>
                                <th>Duyệt</th>
                                <th>Không Hiện</th>
                                <th>Comment</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        @if(Auth::user()->quyen == 3)

                        @else
                        <tbody>
                            @foreach ($tintuc as $tt)
                            <tr class="odd gradeX" align="center">
                                <td>{{ $tt->id }}</td>
                                <td><p>{{ $tt->TieuDe }}</p>
                                    <img src="upload/tintuc/{{ $tt->Hinh }}" width="100px" height="100px" alt="">
                                </td>
                                <td>{{ $tt->TomTat }}</td>
                                <td>{{ $tt->loaitin->theloai->Ten}}</td>
                                <td>{{ $tt->loaitin->Ten }}</td>
                                <td>{{ $tt->KyDanh }}</td>
                                <td>
                                    @if ($tt->Status == 0)
                                        {{ 'Cần duyệt' }}
                                    @else
                                        {{ 'Hiện' }}
                                    @endif
                                </td>
                                <td>
                                    @if ($tt->NoiBat == 0)
                                        {{ 'Không' }}
                                    @else
                                        {{ 'Có' }}
                                    @endif
                                </td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i><a href="admin/tintuc/duyet/{{ $tt->id }}">Duyệt</a>
                                </td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/tintuc/huyduyet/{{ $tt->id }}">Cancel</a>
                                </td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/comment/danhsach/{{ $tt->id }}">Comment</a></td>
                                @if(Auth::user()->quyen == 1)
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/tintuc/xoa/{{ $tt->id }}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/tintuc/sua/{{ $tt->id }}">Edit</a></td>
                                @else
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i>DeleTe</td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i>Edit</td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                        @endif
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

@endsection
