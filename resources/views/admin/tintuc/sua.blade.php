@extends('admin.layout.index')

@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tin Tức
                    <small>{{ $tintuc->TieuDe }}</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                @if (count($errors)>0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $er)
                            {{ $er }}<br>
                        @endforeach
                    </div>
                @endif

                @if (session('thongbao'))
                    <div class="alert alert-success">
                        {{ session('thongbao') }}
                    </div>
                @endif
                <form action="admin/tintuc/sua/{{ $tintuc->id }}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <label>Thể Loại</label>
                        <select class="form-control" name="TheLoai" id="TheLoai">
                            @foreach ($theloai as $tl)
                            <option
                                @if($tintuc->loaitin->theloai->id == $tl->id)
                                {{"selected" }}
                                @endif
                                value="{{ $tl->id }}">{{ $tl->Ten }}</option>
                            @endforeach

                        </select>
                    </div>
                    <div class="form-group"  >
                        <label>Loại Tin</label>
                        <select class="form-control" id="LoaiTin" name="LoaiTin">
                            @foreach ($loaitin as $lt)
                                <option
                                @if($tintuc->loaitin->id == $lt->id)
                                {{"selected" }}
                                @endif
                                value="{{ $lt->id }}">{{ $lt->Ten }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tiêu đề</label>
                        <input class="form-control" name="TieuDe" placeholder="Nhập tiêu đề" value="{{ $tintuc->TieuDe }}"/>
                    </div>

                    <div class="form-group">
                        <label>Tóm tắt</label>
                        <textarea id="demo" class="form-control " name="TomTat">{{ $tintuc->TomTat }}</textarea>
                    </div>

                    <div class="form-group">
                        <label>Video</label>
                        <input class="form-control" name="Video"  value="{{ $tintuc->Video }}" placeholder="Đọc VD-Nhung-Link-Video.docx để sửa video bài viết"/>
                    </div>

                    <div class="form-group">
                        <label>Nội Dung</label>
                        <textarea id="demo" class="form-control ckeditor" name="NoiDung">{{ $tintuc->NoiDung }}</textarea>
                    </div>

                    <div class="form-group">
                        <label>Hình Ảnh</label>
                        <img src="upload/tintuc/{{ $tintuc->Hinh }}" alt="" width="400px" height="400px"><br>
                        <input class="form-control" name="Hinh" type="file"/>
                    </div>

                    <div class="form-group">
                        <label>Nổi Bật</label>
                        <label class="radio-inline">
                            <input
                                @if($tintuc->NoiBat == 0)
                                {{ "checked" }}
                                @endif
                            value="0" checked="" type="radio" name="NoiBat"> Không
                        </label>
                        <label class="radio-inline">
                            <input
                            @if($tintuc->NoiBat == 1)
                            {{ "checked" }}
                            @endif
                            value="1" type="radio" name="NoiBat">Có
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default">Sửa</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                <form>
            </div>
        </div>
        <!-- /.row -->




    <!-- /.container-fluid -->
</div>

@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $("#TheLoai").change(function(){
                var idTheLoai = $(this).val();
                $.get("admin/ajax/loaitin/"+idTheLoai,function(data){
                    $("#LoaiTin").html(data);
                });
            });
        });
    </script>


@endsection
