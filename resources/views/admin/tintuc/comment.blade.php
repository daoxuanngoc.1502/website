@extends('admin.layout.index')

@section('content')

<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Comment
                            <small>Danh Sách</small>
                            <small> {{ $tintuc->TomTat }}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if (session('thongbao'))
                        <div class="alert alert-success">
                            {{ session('thongbao') }}
                        </div>
                    @endif
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr align="center">
                                    <th>ID</th>
                                    <th>Người Dùng</th>
                                    <th>Nội Dung</th>
                                    <th>Ngày Đăng</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @foreach ($tintuc->comment as $cmt)
                                    <tr class="odd gradeX" align="center">
                                        <td>{{ $cmt->id }}</td>
                                        <td>{{ $cmt->user->name }}</td>
                                        <td>{{ $cmt->NoiDung }}</td>
                                        <td>{{ $cmt->created_at }}</td>
                                        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/comment/xoa/{{ $cmt->id }}">Delete</a></td>
                                    </tr>
                                    @endforeach
                            </tbody>

                        </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

@endsection
