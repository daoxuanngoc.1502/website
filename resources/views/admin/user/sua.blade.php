@extends('admin.layout.index')

@section('content')
    
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">User
                    <small>Sửa</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            @if (count($errors)>0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $er)
                            {{ $er }}<br>
                        @endforeach
                    </div>
                @endif

                @if (session('thongbao'))
                    <div class="alert alert-success">
                        {{ session('thongbao') }}
                    </div>
                @endif
            <div class="col-lg-7" style="padding-bottom:120px">
                <form action="admin/user/sua/{{ $user->id }}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <label>Họ Tên</label>
                        <input class="form-control" name="name" value="{{ $user->name }}"/>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input class="form-control" name="email" type="email" value="{{ $user->email }}" readonly/>
                    </div>
                    
                    <div class="form-group">
                        
                        <label>PassWord</label>
                        <input class="form-control" name="password" type="password"/>
                    </div>
                    <div class="form-group">
                        <label>Nhập lại PassWord</label>
                        <<input class="form-control" name="passwordAgain" type="password"/>
                    </div>
                    
                    <div class="form-group">
                        <label>Quyền</label>
                        <label class="radio-inline">
                            <input name="quyen" value="0" 
                            @if ($user->quyen == 0)
                                {{ "checked" }}
                            @endif
                            type="radio">Thường
                        </label>
                        <label class="radio-inline">
                            <input name="quyen" value="1" 
                            @if ($user->quyen == 1)
                                {{ "checked" }}
                            @endif
                            type="radio">Admin
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default">Thêm</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>

@endsection