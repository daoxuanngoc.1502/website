@extends('admin.layout.index')

@section('content')

<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Loại Tin
                            <small>Danh Sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->

                    @if (count($errors)>0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $err)
                            {{ $err }}<br>
                        @endforeach
                        </div>
                    @endif

                    @if (session('thongbao'))
                    <div class="alert alert-success">
                    {{ session('thongbao') }}
                    </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Tên Loại Tin</th>
                                <th>Tên Không Dấu</th>
                                <th>Thể Loại</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        @if(Auth::user()->quyen == 3 or Auth::user()->quyen == 2)

                        @else
                        <tbody>
                            @foreach ($loaitin as $lt)
                            <tr class="odd gradeX" align="center">
                                <td>{{ $lt->id }}</td>
                                <td>{{ $lt->Ten }}</td>
                                <td>{{ $lt->TenKhongDau }}</td>
                                <td>{{ $lt->theloai->Ten }}</td>
                                @if(Auth::user()->quyen == 1)
                                    <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/loaitin/xoa/{{ $lt->id }}"> Delete</a></td>
                                    <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/loaitin/sua/{{$lt->id}}">Edit</a></td>
                                @else
                                    <td class="center"><i class="fa fa-trash-o  fa-fw"></i>DeleTe</td>
                                    <td class="center"><i class="fa fa-pencil fa-fw"></i>Edit</td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                        @endif
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

@endsection
