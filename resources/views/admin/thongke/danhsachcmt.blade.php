@extends('admin.layout.index')

@section('content')

<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Thống Kê
                            <small>Danh sách comment</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if (session('thongbao'))
                        <div class="alert alert-success">
                            {{ session('thongbao') }}
                        </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Thể Loại</th>
                                <th>Loại Tin</th>
                                <th>ID Tin Tức</th>
                                <th>Tiêu Đề Tin Tức</th>
                                <th>Số Comment</th>
                            </tr>
                        </thead>
                        @if(Auth::user()->quyen == 3)

                        @else
                        <tbody>

                            @foreach ($tintuc as $tk)
                            <tr class="odd gradeX" align="center">
                                <td>{{ $tk->loaitin->theloai->Ten }}</td>
                                <td>{{ $tk->loaitin->Ten }}</td>
                                <td>{{ $tk->id }}</td>
                                <td>{{ $tk->TieuDe }}</td>
                                <td>{{ count($tk->comment) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        @endif
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

@endsection
