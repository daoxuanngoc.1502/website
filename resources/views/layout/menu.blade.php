<div class="container-fluid" id="myHeader" style="padding-right: 0 !important;padding-left: 0 !important">
    <div id="menu">
        <ul>
            <li>
                <a href="trangchu" style="position: relative;top: -5px;"><img src="image/home2.png" alt="" width="30px"></a>
            </li>
            @foreach ($theloai as $tl)
                @if (count($tl->loaitin) > 0)
                    <li>
                        <span class="name-product">{{ $tl->Ten }}</span>
                        <ul class="sub-menu">
                            @foreach ($tl->loaitin as $lt)
                                <li>
                                    <a href="loaitin/{{ $lt->id }}/{{ $lt->TenKhongDau }}.html" class="name-product">{{ $lt->Ten }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endif
            @endforeach
            <li>
                <?php
                if(Auth::check())
                {
                    $nguoidung = Auth::user();
                }
                ?>
                @if(!isset($nguoidung))
                    <a href="login" style="position: relative;left: 117px;">
                        <img src="image/login.png" alt="" width="25px" height="25px" >Đăng Nhập
                    </a>
                @else
                    <li>
                        <img src="image/login.png" alt="" style="z-index: 9999;position: relative;top: -2px;" width="25px" height="25px">
                        <span style="color: #fff;font-size: 13px;">{{ $nguoidung->name }}</span></li>
                    <li><a href="dangxuat" ><img src="image/enter.png" alt="" style="z-index: 9999;" width="25px" height="25px"></a></li>
                @endif
            </li>
        </ul>
      </div>
</div>

