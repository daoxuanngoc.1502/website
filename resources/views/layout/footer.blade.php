<footer>
    <div class="main-header" style="margin-top: 40px;" >
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img src="image/logo-300x90.png" alt="" class="img-responsive" style="margin: 50px 0;">
                </div>
                <div class="col-md-4">
                    <div class="p-footer">
                        <p style="font-size: 25px;font-weight: 700;">VỀ CHÚNG TÔI</p><br>
                        <p>Địa chỉ: 319 C16 Lý Thường Kiệt, Phường 15, Quận 11, Tp.HCM</p>
                        <p>Điện thoại: 076 922 0162</p>
                        <p>Email: [email protected]</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="p-footer">
                        <p style="font-size: 25px;font-weight: 700;">THEO DÕI CHÚNG TÔI</p><br>
                    </div>

                </div>
            </div>

        </div>
    </div>
</footer>
<div id="bttop" style="display: block;">
    <i class="fa fa-chevron-up"></i>
</div>
