<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Website</title>
    <base href="{{ asset('') }}">
    <meta name="author" content="ThemePunch" />
    <meta name="description" content="The Garden theme tempalte">
    <meta name="keywords" content="The Garden theme template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="icon" href="images/favicon.ico" type="image/gif" sizes="16x16"> -->
    <!--Icons fonts-->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="vendor/themify-icons/themify-icons.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i|Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

    <!--Styles-->
    <link href="vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/animsition/dist/css/animsition.min.css" rel="stylesheet">
    <link href="vendor/animate.css/source/slide_fwd_center/slide_fwd_center.css" rel="stylesheet">
    <link href="vendor/owl-carousel/css/owl.carousel.css" rel="stylesheet">
    <link href="vendor/css-hamburgers/css/hamburgers.min.css" rel="stylesheet">
    <link href="vendor/slick/css/slick.css" rel="stylesheet">
    <link href="vendor/range_filter/css/jquery-ui.css" rel="stylesheet"> @yield('css')
    <!-- Revolution -->
    <link rel="stylesheet" type="text/css" href="vendor/slider-revolution/css/settings.css">
    <link rel="stylesheet" type="text/css" href="vendor/slider-revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="vendor/slider-revolution/css/navigation.css">
    <!--Theme style-->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="admin_asset/css/style.css">
    @yield('css')
    <style>
        .imgHover {
            -webkit-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -moz-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            text-decoration: none;
            outline: none;
            color: inherit;
        }

        .imgHover:hover {
            -ms-transform: scale(1.5);
            /* IE 9 */
            -webkit-transform: scale(1.5);
            /* Safari 3-8 */
            transform: scale(1.05);
            outline: 0;
        }

        a:hover {
            transform: scale(1.05);
        }

        .line-eee {
            margin-top: 5px !important;
            margin-bottom: 20px;
            border: 0;
            border-top: 7px solid #eee;
        }
        /*==Style cho menu===*/

        #menu ul {
            z-index: 999999;
            background: #A92720;
            list-style-type: none;
            text-align: center;
        }

        #menu li {
            color: #f1f1f1;
            display: inline-block;
            width: 120px;
            height: 40px;
            line-height: 40px;
            margin-left: -5px;
        }

        #menu a {
            text-decoration: none;
            color: #fff;
            display: block;
        }
        /*==Dropdown Menu==*/

        .sub-menu {
            display: none;
            position: absolute;
        }

        #menu li {
            position: relative;
        }

        #menu li:hover .sub-menu {
            display: block;
        }

        .sub-menu li {
            margin-left: 0 !important;
            width: 200px !important;
            text-align: justify !important;
            padding-left: 15px;
        }
        /*==Menu cấp 3==*/

        .sub-menu>ul {
            display: none !mportant;
        }
        /*BACK TO TOP*/
        #bttop {
            text-align: center;
            width: 40px;
            height: 40px;
            line-height: 40px;
            position: fixed;
            bottom: 49px;
            right: 10px;
            cursor: pointer;
            display: none;
            color: #fff;
            font-size: 11px;
            font-weight: 900;
            text-transform: uppercase;
            z-index: 999999;
            -webkit-border-radius: 10%;
            -moz-border-radius: 10%;
            -o-border-radius: 10%;
            border-radius: 10%;
            opacity: 0.8;
            background: #60bc18;
        }

        .p-sub{
            font-weight: 700;
            font-size: 20px;
        }

    </style>
</head>

<body>

    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=324083185263486&autoLogAppEvents=1" nonce="7VZKwUrg">
    </script>

    <div>
        @php
            session(['getUrl' => URL::current()]);
        @endphp

        @include('layout.header')
        <section class="">
            <div class="container">
                <form action="timkiem" method="POST">
                    <input type="hidden" type="_token" value="{{ csrf_token() }}";>
                    <div style="padding-top: 20px;padding-bottom:20px">
                        @csrf
                        <input type="text" class="form-control" placeholder="Search..." name="tukhoa">
                        <span class="input-group-btn" style="top: -34px;left: -27px;float: right;">
                            <button class="btn btn-default" type="submit" style="height: 34px;">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
            </div>
        </section>
        @yield('content')
        @include('layout.footer')

        <script src="vendor/jquery/dist/jquery.min.js"></script>
        <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendor/jquery_easing/jquery.easing.min.js"></script>
        <script src="vendor/owl-carousel/js/owl.carousel.js"></script>
        <script src="vendor/slick/js/slick.js"></script>
        <script src="vendor/isotope/js/isotope.js"></script>
        <script src="vendor/isotope/js/imagesloaded.pkgd.js"></script>
        <script src="vendor/range_filter/js/jquery-ui.js"></script>
        <script type="text/javascript" src="vendor/slider-revolution/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="vendor/slider-revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script type="text/javascript" src="vendor/slider-revolution/js/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="vendor/slider-revolution/js/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="vendor/slider-revolution/js/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="vendor/slider-revolution/js/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="vendor/slider-revolution/js/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="vendor/slider-revolution/js/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="vendor/slider-revolution/js/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="vendor/slider-revolution/js/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="vendor/slider-revolution/js/revolution.extension.video.min.js"></script>

        <script src="script/main.js"></script>

        @yield('script')
</body>

</html>
