<?php

use App\TheLoai;
use Illuminate\Support\Facades\Route;
use App\LoaiTin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('admin/dangnhap','UserController@getDangNhapAdmin');
Route::post('admin/dangnhap','UserController@postDangNhapAdmin');
Route::get('admin/logout','UserController@getDangXuatAdmin');

Route::get('trangchu','PagesController@trangchu');
Route::get('loaitin/{id}/{TenKhongDau}.html','PagesController@loaitin');
Route::get('tintuc/{id}/{TenKhongDau}.html','PagesController@tintuc')->name('urlTinTuc');
Route::get('login','PagesController@getLogin');
Route::post('login','PagesController@postLogin');
Route::get('dangky','PagesController@getDangKy');
Route::post('dangky','PagesController@postDangKy');
Route::get('dangxuat','PagesController@getDangXuat');
Route::post('comment/{id}','CommentController@postComment');
Route::get('login/{social}', 'PagesController@redirectToProvider');
Route::get('login/{social}/callback', 'PagesController@handleProviderCallback');
// Route::get('timkiem', 'PagesController@getTimKiem');
Route::post('timkiem', 'PagesController@postTimKiem');

Route::group(['prefix' => 'admin','middleware'=>'adminLogin'], function () {
    Route::group(['prefix' => 'theloai'], function () {
        Route::get('danhsach', 'TheLoaiController@getDanhSach');

        Route::get('sua/{id}', 'TheLoaiController@getSua');
        Route::post('sua/{id}', 'TheLoaiController@postSua');


        Route::get('them', 'TheLoaiController@getThem');
        Route::post('them', 'TheLoaiController@postThem');


        Route::get('xoa/{id}', 'TheLoaiController@getXoa');


    });

    Route::group(['prefix' => 'loaitin'], function () {
        Route::get('danhsach', 'LoaiTinController@getDanhSach');

        Route::get('sua/{id}', 'LoaiTinController@getSua');
        Route::post('sua/{id}', 'LoaiTinController@postSua');

        Route::get('them', 'LoaiTinController@getThem');
        Route::post('them', 'LoaiTinController@postThem');

        Route::get('xoa/{id}', 'LoaiTinController@getXoa');


    });

    Route::group(['prefix' => 'tintuc'], function () {
        Route::get('danhsach', 'TinTucController@getDanhSach');

        Route::get('sua/{id}', 'TinTucController@getSua');
        Route::post('sua/{id}', 'TinTucController@postSua');

        Route::get('them', 'TinTucController@getThem');
        Route::post('them', 'TinTucController@postThem');

        Route::get('xoa/{id}', 'TinTucController@getXoa');
        Route::get('duyet/{id}', 'TinTucController@getDuyet');
        Route::get('huyduyet/{id}', 'TinTucController@getHuyDuyet');
    });

    Route::group(['prefix' => 'thongke'], function () {
        Route::get('danhsach', 'thongKeController@getDanhSach');
        Route::get('danhsachcmt', 'thongKeController@getDanhSachCmt');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('danhsach', 'UserController@getDanhSach');

        Route::get('sua/{id}', 'UserController@getSua');
        Route::post('sua/{id}', 'UserController@postSua');

        Route::get('them', 'UserController@getThem');
        Route::post('them', 'UserController@postThem');

        Route::get('xoa/{id}', 'UserController@getXoa');
    });

    Route::group(['prefix' => 'comment'], function () {
        Route::get('danhsach/{id}', 'CommentController@getComment');

        Route::get('xoa/{id}', 'CommentController@getXoa');
    });

    Route::group(['prefix' => 'ajax'], function () {
        Route::get('loaitin/{idTheLoai}','AjaxController@getLoaiTin');
    });
});

Route::group(['prefix' => 'client'], function () {

});

